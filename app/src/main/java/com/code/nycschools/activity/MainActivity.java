package com.code.nycschools.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.code.nycschools.adapter.SchoolAdapter;
import com.code.nycschools.data.SAT_Info;
import com.code.nycschools.R;
import com.code.nycschools.data.School_Info;
import com.code.nycschools.network.DataCall;
import com.code.nycschools.network.NetworkCheck;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private String SCHOOL;
    private String SAT;

    private String baseURL;
    private School_Info schoolSelected;
    private String boro;

    private List<School_Info> schoolList;

    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baseURL = getResources().getString(R.string.base_api_url);
        SCHOOL = getResources().getString(R.string.school_call);
        SAT = getResources().getString(R.string.sat_call);

        boro = "M";  // default startup borough
        TextView tabLabel = findViewById(R.id.manhattan);
        tabLabel.setBackgroundColor(Color.RED);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (new NetworkCheck().netCheck(this)) new DataCall().getSchoolList(this, baseURL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        finish();
    }

    public void onSchoolListReceived(final Response<List<School_Info>> response) {
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

        Comparator<School_Info> compareBySchoolName = (School_Info o1, School_Info o2) ->
                o1.getSchoolName().compareTo(o2.getSchoolName());

        this.schoolList = response.body().stream().sorted(compareBySchoolName).collect(Collectors.toList());

        filterAndDisplay();
    }

    public void onSatInfoReceived(Response<List<SAT_Info>> response) {
        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

        satAlertDialog(response.body().get(0));
    }

    public void filterAndDisplay() {

        List<School_Info> schoolBoroFiltered =
                schoolList.stream().filter(school-> school.getBoro().equals(boro)).collect(Collectors.toList());

        ListView listView = findViewById(R.id.school_list);
        listView.setAdapter(new SchoolAdapter(context, schoolBoroFiltered));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                satSelect(schoolBoroFiltered.get(position));
            }
        });
    }

    public void setBorough(View view) {
        resetTabs(view);
        view.setBackgroundColor(Color.RED);
        boro = view.getTag().toString();

        filterAndDisplay();
    }

    private void resetTabs(View view) {
        LinearLayout linearLayout = (LinearLayout) view.getParent();

        for (int index = 0; index < linearLayout.getChildCount(); index++ ) {
            linearLayout.getChildAt(index).setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void satSelect(School_Info schoolSelected) {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        this.schoolSelected = schoolSelected;

        new DataCall().getSatInfo(this, baseURL, schoolSelected.getDbn());
    }

    private void satAlertDialog(SAT_Info satInfo) {
        String satMessage;

        if (satInfo != null) {
            satMessage = "\nSAT Info for school:\n"
                    + "\nMath Score: " + satInfo.getSatMathAvgScore()
                    + "\nReading Score: " + satInfo.getSatCriticalReadingAvgScore()
                    + "\nWriting Score: " + satInfo.getSatWritingAvgScore();
        } else {
            satMessage = "\nNo SAT Info available for school\n";
        }

        new AlertDialog.Builder(this)
                .setTitle(schoolSelected.getSchoolName())
                .setMessage(satMessage)
                .setPositiveButton(getString(R.string.dismiss_alert), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).show();
    }

}
