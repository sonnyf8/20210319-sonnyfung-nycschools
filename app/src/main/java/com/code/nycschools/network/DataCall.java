package com.code.nycschools.network;

import android.content.Context;

import com.code.nycschools.activity.MainActivity;
import com.code.nycschools.data.SAT_Info;
import com.code.nycschools.data.School_Info;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataCall {
    public void getSchoolList(final Context context, String baseUrl) {
        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

        SchoolCalls schoolCalls = retrofit.create(SchoolCalls.class);
        Call<List<School_Info>> schoolCall = schoolCalls.getSchools();

        schoolCall.enqueue(new Callback<List<School_Info>>() {
            @Override
            public void onResponse(Call<List<School_Info>> call, Response<List<School_Info>> response) {
                ((MainActivity) context).onSchoolListReceived(response);
            }

            @Override
            public void onFailure(Call<List<School_Info>> call, Throwable t) { }
        });
    }

    public void getSatInfo(final Context context, String baseUrl, String dbn) {
        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        SchoolCalls schoolCalls = retrofit.create(SchoolCalls.class);
        Call<List<SAT_Info>> schoolCall = schoolCalls.getSatInfo(dbn);

        schoolCall.enqueue(new Callback<List<SAT_Info>>() {
            @Override
            public void onResponse(Call<List<SAT_Info>> call, Response<List<SAT_Info>> response) {
                ((MainActivity) context).onSatInfoReceived(response);
            }

            @Override
            public void onFailure(Call<List<SAT_Info>> call, Throwable t) { }
        });
    }
}
