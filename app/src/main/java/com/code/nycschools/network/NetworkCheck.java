package com.code.nycschools.network;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.code.nycschools.R;

public class NetworkCheck {

    public boolean netCheck(final AppCompatActivity appCompatActivity) {
        ConnectivityManager cm = (ConnectivityManager) appCompatActivity.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
            if (cm.getActiveNetworkInfo().isConnected()) {return true;}
        }

        new AlertDialog.Builder(appCompatActivity)
                .setTitle(appCompatActivity.getString(R.string.network_error))
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setMessage(appCompatActivity.getString(R.string.check_settings))
                .setPositiveButton(appCompatActivity.getString(R.string.exit_app), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appCompatActivity.finish();
                    }
                }).setCancelable(false).show();

        return false;
    }

}
