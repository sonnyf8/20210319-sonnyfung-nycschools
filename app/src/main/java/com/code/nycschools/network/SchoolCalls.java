package com.code.nycschools.network;

import com.code.nycschools.data.SAT_Info;
import com.code.nycschools.data.School_Info;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface SchoolCalls {

    @GET("resource/s3k6-pzi2.json")
    Call<List<School_Info>> getSchools();

    @GET("resource/f9bf-2cp4.json")
    Call<List<SAT_Info>> getSatInfo(@Query("dbn") String dbn);

}
