package com.code.nycschools.adapter;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.code.nycschools.BR;
import com.code.nycschools.R;
import com.code.nycschools.data.School_Info;

import java.util.List;

public class SchoolAdapter extends BaseAdapter {
    private Context context;
    private List<School_Info> schoolBoroList;

    public SchoolAdapter(Context context, List<School_Info> schoolBoroList) {
        this.context = context;
        this.schoolBoroList = schoolBoroList;
    }

    @Override
    public int getCount() {
        return schoolBoroList.size();
    }

    @Override
    public Object getItem(int position) {
        return schoolBoroList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewDataBinding rowViewBinding;

        if (convertView == null) {
            rowViewBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.school_row, parent, false);
        } else {
            rowViewBinding = DataBindingUtil.bind(convertView);
        }

        rowViewBinding.setVariable(BR.schoolInfo, schoolBoroList.get(position));
        return rowViewBinding.getRoot();
    }

}
